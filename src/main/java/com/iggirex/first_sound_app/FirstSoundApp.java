package com.iggirex.first_sound_app;

import javax.sound.midi.*;

public class FirstSoundApp {

    static final int INSTRUMENT = 9;
    static final int NOTE = 44;
    static final int VELOCITY = 127;

    static final int START_TICK = 1;
    static final int START_COMMAND = 144;
    static final int END_TICK = 16;
    static final int END_COMMAND = 128;


    public static void main(String[] args) {
        FirstSoundApp app = new FirstSoundApp();
        app.play();
    }

    public void play() {

        try {
            Sequencer player = MidiSystem.getSequencer();
            player.open();

            Sequence seq = new Sequence(Sequence.PPQ, 4);
            Track track = seq.createTrack();

            ShortMessage a = new ShortMessage();
            a.setMessage(START_COMMAND, INSTRUMENT, NOTE, VELOCITY);
            MidiEvent noteOn = new MidiEvent(a, START_TICK);
            track.add(noteOn);

            ShortMessage b = new ShortMessage();
            b.setMessage(END_COMMAND, INSTRUMENT, NOTE, VELOCITY);
            MidiEvent noteOff = new MidiEvent(b, END_TICK);
            track.add(noteOff);

            player.setSequence(seq);

            player.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
